FROM golang:1.14.2-alpine3.11 as build

RUN apk --no-cache add git make musl-dev gcc \
        && go get -v github.com/xperimental/nextcloud-exporter

FROM alpine:3.11
RUN apk --no-cache add ca-certificates
COPY --from=build /go/bin/nextcloud-exporter /usr/local/bin/nextcloud-exporter
EXPOSE 9205
ENTRYPOINT ["/usr/local/bin/nextcloud-exporter"]
